class User < ActiveRecord::Base

    has_many :likes
    has_many :posts, through: :likes

    EMAIL_REGEX = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]+)\z/i

        validates :name, presence: true,
                                length: {within: 2..50}

        validates :alias, presence: true,
                                length: {within: 2..50}
        
        validates :email, presence: true,
                            format: {with: EMAIL_REGEX, message: "incorrect email format"},
                            uniqueness: {case_sensitive: false}
        
        validates :password, presence: true,
                                length: {within: 6..20}

        before_save {self.email = email.downcase}
        has_secure_password


        # attr_accessor :email, :name, :password, :password_confirmation
        # attr_accessible :email, :name, :password, :password_confirmation
        # has_secure_password

    end
    