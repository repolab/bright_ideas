class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_filter :initialize_user
  
    def initialize_user
      @user = User.new
    end

  protect_from_forgery with: :exception
end
