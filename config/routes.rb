Rails.application.routes.draw do
  root "users#index"
  resources :sessions
  resources :users
  resources :posts
end
